#!/usr/bin/python

from setuptools import setup

import glob

setup(
    name             = 'gearshare',
    version          = '0.1',
    url              = 'none',
    author           = 'Bryce Harrington',
    author_email     = 'bryce@bryceharrington.org',
    description      = 'Lending library for cloud hosts',
    platforms        = ['any'],
    requires         = ['pydoctor', 'pep8', 'pyflakes'],
    setup_requires   = ['pytest-runner'],
    packages         = [
        'gearshare',
        ],
    package_data = { },
    data_files = [ ],
    scripts = glob.glob('scripts/*'),

    tests_require = ["pytest"],
)
