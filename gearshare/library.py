#!/usr/bin/python3

import os
import sys
from datetime import datetime

sys.path.insert(0, os.path.realpath(
    os.path.join(os.path.dirname(__file__), "..")))


class Resource:
    """
    Base class for Library managed items.

    Instances of this superclass must have a unique id string for
    referencing purposes.
    """
    UNKNOWN = 0
    AVAILABLE = 1
    CHECKED_OUT = 2

    def __init__(self, uuid):
        self._uuid = uuid
        self.reservations = []

    def uuid(self):
        return self._uuid

    def status(self):
        pass

    def reserve(self, user, start_date, duration):
        pass

    def checkout(self, user, duration):
        return self.reserve(user, datetime.now(), duration)

    def checkin(self):
        pass


class Library:
    """
    A collection of items that can be checked in and out.

    Items can be checked in/out and renewed to individual patrons.
    Only one patron can check out an item at a time.
    """
    def __init__(self):
        self.resources = {}

    def add(self, item):
        if (type(item) == list):
            for i in item:
                self.add(i)
            return True
        assert(isinstance(item, Resource))

        # Don't add duplicates of a given resource
        if item.uuid() in self.resources.keys():
            return False

        self.resources[item.uuid()] = item
        return True

    def remove(self, uuid):
        if uuid not in self.resources.keys():
            return False
        del self.resources[uuid]

    def get(self, uuid):
        return self.resources.get(uuid, None)

    def search(self):
        return self.resources


if (__name__ == "__main__"):
    import uuid
    import pprint
    pp = pprint.PrettyPrinter(indent=4)

    class Book(Resource):
        def __init__(self, uuid, name):
            Resource.__init__(self, uuid)
            self.name = name

        def __repr__(self):
            return self.name

    test_book_1_uuid = str(uuid.uuid4())
    test_book_1 = Book(test_book_1_uuid, "Test Book 1")
    print("Book:")
    pp.pprint(test_book_1.__dict__)

    library = Library()
    library.add(test_book_1)
    print("\nLibrary:")
    pp.pprint(library.__dict__)

    test_book_2 = Book(str(uuid.uuid4()), "Test Book 2")
    test_book_3 = Book(str(uuid.uuid4()), "Test Book 3")
    test_book_4 = Book(str(uuid.uuid4()), "Test Book 4")
    library.add([test_book_2, test_book_3, test_book_4])

    book = library.get(test_book_1_uuid)
    print("\nRetrieved book:")
    pp.pprint(book.__dict__)

    library.remove(test_book_1_uuid)

    print("\nInventory:")
    pp.pprint(library.search())

